#pragma once

#include <atomic>
#include <cstdint>
#include <mutex>
#include <condition_variable>

//#define USE_STD_SYNC

#ifdef USE_STD_SYNC
#include <semaphore>
#include <barrier>
#endif


using std::size_t;

namespace kppr::sync {

    template <size_t Max_Value>
    class Semaphore {
#ifdef USE_STD_SYNC
        std::counting_semaphore<Max_Value> sem;
#else
        std::atomic<size_t> value;
        std::mutex mutex;
        std::condition_variable cond;
#endif

    public:
#ifdef USE_STD_SYNC
        Semaphore(size_t initial) : sem(initial) {}
#else
        Semaphore(size_t initial) : value(initial) {}
#endif

        inline void release()
        {
#ifdef USE_STD_SYNC
            this->sem.release();
#else
            this->value++;
            this->cond.notify_one();
#endif
        }

        inline bool try_acquire()
        {
#ifdef USE_STD_SYNC
            return this->sem.try_acquire();
#else
            std::lock_guard lock(this->mutex);
            if (this->value) {
                this->value--;
                return true;
            } else
                return false;
#endif
        }

        inline void acquire()
        {
#ifdef USE_STD_SYNC
            this->sem.acquire();
#else
            std::unique_lock lock(this->mutex);
            this->cond.wait(lock, [this] { return this->value > 0; });
            this->value--;
#endif
        }
    };


    template <typename CompletionFunction>
    class Barrier {
#ifdef USE_STD_SYNC
        std::barrier<CompletionFunction> barrier;
#else
        CompletionFunction comp_func;
        size_t expected;
        size_t count;
        size_t phase;
        std::mutex mutex;
        std::condition_variable cond;
#endif

    public:

#ifdef USE_STD_SYNC
        Barrier(size_t a_expected, CompletionFunction f = [](){}) : barrier(a_expected, f) {}
#else
        Barrier(size_t a_expected, CompletionFunction f = [](){}) : comp_func(f),
                                                                    expected(a_expected),
                                                                    count(a_expected),
                                                                    phase(0) {}
#endif

        inline void arrive_and_wait()
        {
#ifdef USE_STD_SYNC
            barrier.arrive_and_wait();
#else
            std::unique_lock lock(this->mutex);
            size_t my_phase = phase;
            this->count--;
            if (count)
                this->cond.wait(lock, [this, my_phase] { return this->phase != my_phase; });
            else {
                this->comp_func();
                this->count = expected;
                this->phase++;
                this->cond.notify_all();
            }
#endif
        }

    };

}

