\section{Úvod}

Statistický pojem \emph{percentil} označuje hodnotu ze statistického souboru, která je větší než dané procento všech hodnot tohoto souboru. Je jednou z charakteristik, které umožňují utvářet generickou představu o podobě statistického souboru.

Zadáním této semestrální práce je v programovacím jazyce C++ implementovat program využívající metod paralelizace, který v libovolně velkém souboru nalezne zadaný percentil.


\section{Analýza}

V diskrétních souborech lze zcela naivně percentil nalézt utvořením uspořádané $n$-tice všech hodnot souboru seřazených podle velikosti od nejmenší po největší a výběrem hodnoty na pozici této $n$-tice získané vynásobením celkového počtu hodnot poměrem určeným indexem percentilu vyděleným stem. Tj. například pozici percentilu 32 lze vyjádřit jako $n \cdot 0.32$.

Tento způsob hledání percentilu je však nepraktický až zcela nepoužitelný, obzvláště pak pro velké vstupní soubory dat. V informatice naráží především na velké paměťové nároky.


\subsection{Histogram}

Jistou kompresi dat nám v případě diskrétních souborů, se kterými se setkáváme v počítačových systémech, může poskytnout nahrazení seřazené uspořádané $n$-tice seřazeným histogramem. Namísto $k$ opakování hodnoty $x$ v $n$-tici tak lze uložit informaci, že hodnota $x$ se v souboru nachází $k$-krát. Percentil pak nalezneme sčítáním četností hodnot od počátku histogramu, dokud jejich součet není větší nebo roven požadované pozici percentilu v $n$-tici.

I v tomto případě však narážíme na paměťové nároky, jelikož v případě 64bitových čísel můžeme pro takový soubor potřebovat až $2^{64}$ datových struktur vyjadřujících dané četnosti. Je nutno tedy histogram dále zmenšit.

Možným řešením je namísto jednoho \uv{velkého} histogramu utvořit hierarchii několika histogramů \uv{menších}. Namísto histogramu jednotlivých hodnot utváříme histogramy pevného počtu \emph{intervalů}. Percentil v takovém histogramu hledáme stále stejným způsobem, pouze s tím rozdílem, že nalezenou hodnotou je interval $I$, který je potřeba dále zpřesnit. To provedeme tak, že vytvoříme histogram stejného počtu menších intervalů, do kterého vkládáme pouze hodnoty, které náleží původnímu nalezenému intervalu $I$. Při hledání percentilu pak nesmíme zapomenout k celkovému součtu četností přičíst součet všech četností hodnot ze souboru, které jsou menší než spodní hranice intervalu $I$.

Zpřesňování opakujeme, dokud nejsou intervaly v histogramu dostatečně malé, aby mohly zastupovat jednotlivé hodnoty souboru.


\subsection{Čísla s plovoucí čárkou}

Vyjadřování intervalů pomocí binárních čísel s plovoucí čárkou představuje jistá úskalí. Přesnost, s jakou lze hodnoty pomocí nich vyjadřovat, totiž není po jejich projekci na reálná čísla rovnoměrně rozdělená. Například mezi čísly $0.0$ a $1.0$ není stejné množství možných binárních hodnot, jako mezi čísly $10000.0$ a $10001.0$. Chceme-li získat přesnou hodnotu percentilu, je problém pro takovou reprezentaci intervalů určit podmínku pro ukončení zpřesňování histogramu.

Jedno z řešení využívá tzv. \emph{type punning}, tj. použití binární hodnoty jednoho datového typu jako hodnoty datového typu odlišného. V našem případě použijeme bitovou reprezentaci čísla s plovoucí čárkou $a_f$ jako bitovou reprezentaci celého čísla $a_i$ o stejném počtu bitů. Čísla $a_f$ a $a_i$ jsou si tedy z pohledu hodnot jejich bitů rovna, ale jejich bity jsou použity k odlišným reprezentacím. Výhoda binárních celých čísel je, že po projekci na reálná čísla jsou na tomto oboru rozdělena rovnoměrně.

Díky uspořádání bitů standardu IEEE-754 lze (až na některé výjimky) takováto čísla analogicky porovnávat. Tedy pro čísla s plovoucí čárkou $a_f$, $b_f$ a jejich celočíselné reprezentace $a_i$, $b_i$ zpravidla platí, že pokud $a_f < b_f$, pak též $a_i < b_i$.

První výjimku tvoří speciální hodnoty čísel s plovoucí čárkou, jako je \emph{nekonečno} a \emph{NaN} -- zadání práce však explicitně káže tyto typy hodnot ignorovat, není tedy nutné se jejich reprezentací zabývat. Druhou výjimkou jsou hodnoty $+0.0$ a $-0.0$, jež v IEEE-754 představují odlišné hodnoty -- i na tento případ však zadání práce pamatuje a káže obě hodnoty považovat za rovné.

Pro tuto práci nejdůležitější výjimkou je případ záporných čísel. V IEEE-754 se od sebe vzájemně opačná čísla liší pouze znaménkovým bitem. Obvyklá reprezentace záporných celých čísel v počítači však využívá dvojkový doplněk, tj. všechny bity opačného čísla jsou převráceny a je k nim přičtena 1. To má důsledek takový, že porovnávání záporných celých čísel a záporných čísel s plovoucí čárkou funguje \emph{opačně}, tedy jestliže $a_f < b_f$, pak $a_i > b_i$). Pro řešení úlohy se nejedná o zásadní překážku, ale je nutné tento fakt vzít při implementaci v potaz.

S použitím této reprezentace čísel se algoritmus zpřesňování histogramu opakuje do chvíle, kdy je znám výsledek percentilu pro histogram s velikostí jednotlivých intervalů rovnou jedné (tj. pro každý interval existuje právě jedna hodnota, kterou je možné do něj vložit).


\begin{figure}[p!]
    \includegraphics[width=0.9\linewidth]{dataflow-final.pdf}
    \caption{Data flow diagram}
\end{figure}

\begin{figure}[p!]
    \includegraphics[width=0.9\linewidth]{ctlflow-final.pdf}
    \caption{Control flow diagram}
\end{figure}


\section{Implementace a optimalizace sériového výpočtu}

Na počátku práce byl kladen důraz na co nejdokonalejší implementaci algoritmu v sériové podobě, která by pak tvořila základ pro zbylé dvě podoby. Implementace je navržena tak, že programátor může pomocí konstanty určit maximální velikost všech hodnot uložených v histogramu v bajtech -- tímto způsobem lze zajistit, že práce vždy splní omezení na paměťové nároky určené zadáním. 

Teoretická myšlenka byla taková, že bude možné v programu vyvažovat mezi rychlostí a paměťovými nároky. Větší histogram by totiž znamenal, že budeme potřebovat menší počet zpřesňování. Ne zcela překvapivě je však praxe mírně odlišná, což je rozebráno dále.

Díky využití \emph{type punningu} je velmi snadné určit velikost intervalů pro daný histogram: je jí celkový rozsah histogramu vydělený maximálním počtem intervalů v histogramu.


\subsection{Binární vyhledávání}

Původní implementace utvářela histogram tak, že četnosti a hodnoty intervalů byly uloženy v seřazeném poli. Program pomocí binárního vyhledávání pro každé vkládané číslo hledal již existující interval. Pokud jej našel, inkrementoval jeho četnost a aktualizoval pozice hodnoty v souboru. Pokud se potřebný interval v histogramu ještě nenacházel, byl na potřebné místo vložen -- tj. byl nalezen index, na který daný interval patří tak, aby histogram zůstal seřazený, hodnoty za od tohoto indexu dále byly pomocí funkce \texttt{std::memmove} posunuty o jeden index doprava a uvolněné místo bylo inicializováno novým intervalem.

Samotné vyhledání pozice vkladu tímto způsobem mělo tedy časovou složitost $O(\log{n})$. Posunutí hodnot má rovněž své výkonnostní implikace a v nejhorším případě, kdy je interval vkládán na počátek pole, má časovou složitost $O(n)$, byť je tato operace ve standardní knihovně zpravidla silně optimalizovaná.

Zpracování souboru o velikosti $2.6$~GiB touto implementací s histogramem o maximální velikosti $16~384$ intervalů na mém stroji trvalo zhruba $47$ sekund. Ukázalo se rovněž, že při použití binárního vyhledávání je mnohem lepší vícekrát zpřesnit menší histogram, než méněkrát zpřesnit větší histogram. To proto, že s velikostí histogramu rapidně narůstaly výpočetní nároky na binární vyhledání pozice, potažmo vklad do histogramu, který je koneckonců v programu tou nejčastější operací.


\subsection{Přímý vklad}

Díky použití \emph{type punningu} lze však použít způsob vkladu do histogramů, který má časovou složitost $O(1)$. Jelikož známe maximální velikost histogramu a přesné velikosti intervalů, lze z jednotlivých vkládaných hodnot snadno vypočítat index intervalu, jehož četnost se má inkrementovat.

Touto změnou bylo zpracování stejného souboru s histogramem o velikosti $16~384$ intervalů zkráceno ze $47$ sekund na $15$ sekund. Pro tento způsob vkladu by již teoreticky mohlo být výhodnější zvětšení histogramu. Použití $131~072$ hodnot zpracování zkrátilo zhruba o další 3 sekundy, tj. z $15$ na $12$.

Jelikož takovýto histogram stále nedosahoval maximální povolené paměťové náročnosti, byly testovány i větší. Zde se však teorie setkává s praxí. Kvůli z velké části náhodnému charakteru čísel v testovacích souborech docházelo k velkému rozpytlu náhodných přístupů k paměti histogramu, což vede k velkému množství chyb stránek. Procesor tedy musí neustále vyměňovat paměťové oblasti ve své cache, což nezanedbatelnou měrou snižuje výkonnost celého programu. Velikost histogramu v rámci nižších jednotek mebibajtů se po četných testech ukázala (minimálně na mém stroji) jako optimální.


\subsection{Další optimalizace}

Program pro svou operaci nepoužívá pouze čistý type punning. Často, především u záporných čísel, je nutné hodnoty pro účely výpočtů převádět na takové, s jakými umí cílová architektura procesoru nativně pracovat. V jazyce C++ pro tyto účely byly implementovány funkce, které tyto převody a výpočty provádějí.


\subsubsection*{Inlining}

Tyto operace zpravidla nebývají samy o sobě nijak náročné, avšak pokud jsou definovány jako funkce, má to na výkon značný vliv kvůli režii spojené s voláním funkce. Použití klíčového slova \texttt{inline} u těch nejčastějších značně celý program urychlilo -- až o $40$~\%.


\subsubsection*{Přeskočení drahé kontroly rozsahu}

Při vkládání každé hodnoty do histogramu dochází k porovnání hodnoty s rozsahem histogramu. Toto porovnání je výkonnostně kvůli převodům čísel do vhodných formátů celkem drahé. Proto je toto porovnání u histogramu nad kompletní škálou čísel zcela přeskočeno, což zrychlilo běh programu.


\subsubsection*{Nahrazení aritmetických operací bitovými}

Původní program pro převod formátů čísel používal negace čísel s plovoucí čárkou k odstranění znaménkového bitu. Tato operace byla později nahrazena jednoduchým bitovým maskováním, které je značně rychlejší.


\subsubsection*{Fyzická pozice dat v paměti}

Při implementaci se ukázalo jako výhodné mít co nejvíce dat v paměti \uv{pohromadě}. Jelikož jsou histogramy a buffery pro načítání dat ze souboru celkem velké, není možné je alokovat na zásobníku. Pomocí standardní třídy \texttt{unique\_ptr} byla tedy paměť těchto struktur alokována na haldě.

Ukázalo se, že je značně výhodnější načítací buffer a histogram umístit do společné struktury a tu pak na haldě alokovat najednou, než alokovat paměť pro tyto dvě struktury zvlášť. U oddělených alokací totiž docházelo k mnohem častějším chybám stránek. Když byly umístěny v jedné struktuře, byly v paměti blízko sebe, čímž došlo ke zrychlení.


\subsection{Výsledek optimalizací}

Zpracování souboru o velikosti $2.6$~GiB se podařilo v sériové podobě zrychlit z původních $47$ sekund na $4 - 8$ sekund. Rychlost je v sériové variantě velmi závislá na vzdálenosti požadovaného percentilu od hodnoty $50$.


\section{Symetrický multiprocesor}

Paralelizace na symetrickém multiprocesoru (SMP) je v této práci implementována tak, že hlavní vlákno vytvoří $n$ zpracovacích vláken, kterým přiděluje části souboru ke zpracování. Každé ze zpracovacích vláken si vytváří svůj oddělený histogram. Jednotlivé histogramy jsou po dokončení čtení souboru před zpřesněním vždy spojeny do jednoho. Po spojení a nalezení percentilu se algoritmus opakuje, dokud velikost intervalu v histogramu není rovna jedné.

Původní implementace přidělovala zpracovacím vláknům části vstupního souboru tak, že hlavní vlákno bylo \emph{producentem} a vkládalo do jediného společného kruhového bufferu přečtené části souboru. Jednotlivá zpracovací vlákna byla \emph{konzumenty} a části vstupního souboru si z bufferu sama vybírala a zpracovávala. Tímto způsobem však občas docházelo k uváznutí.

Konečná implementace vytváří jeden kruhový buffer pro každé vlákno. Hlavní vlákno pak střídá, do kterého bufferu nově přečtená data vloží. Tímto způsobem nedochází ke kolizím mezi jednotlivými konzumenty. Rovněž se takto nemůže stát, že některá vlákna \uv{odvedou více práce} než jiná.

Každá iterace před zpřesňováním histogramu končí pro všechna vlákna čekáním na \emph{bariéře}. Ukončovací funkce bariéry je zodpovědná za spojení histogramů do jednoho a připravení dat pro další iteraci. Jelikož je histogram v paměti uložen jako struktura polí a při spojování dochází k prostému součtu četností jednotlivých intervalů, je umožněno překladači spojování histogramů urychlit pomocí SIMD instrukcí.

Zpracování souboru o velikosti $2.6$~GiB zrychluje použití SMP se dvěma zpracovacími vlákny ze $4 - 8$ sekund na $3 - 4$ sekundy; při čtyřech zpracovacích vláknech pak na konzistentní $3$ sekundy. Vyšší počet vláken již pak nemá měřitelný kladný vliv na výkon, spíše se pak zvyšuje režie na jejich plánování a spojování histogramů a výkon klesá. 


\section{OpenCL -- výpočet na GPGPU}

Původní plán byl jako třetí formu paralelizace využít MPI, a to hlavně proto, že původní způsob vkládání dat do histogramu za použití binárního vyhledávání nebyl příliš vhodný pro zpracování na grafických čipech, u jejichž programů není vhodné dělat četná podmíněná větvení, neboť jsou optimalizovány pro masivní SIMD paralelizaci. Změnou způsobu vkládání na přímý přístup však již díky podpoře OpenCL pro atomické operace nic nebrání implementaci pro GPU, proto bylo původní rozhodnutí změněno.

Způsob výpočtu na GPGPU je velmi podobný tomu sériovému s tím rozdílem, že jednotlivé instance OpenCL kernelu -- tj. hlavní funkce, která je volána z hostitelského kódu -- co nejvíce paralelně zpracovávají jednotlivé hodnoty ze vstupního bufferu. Za použití funkcí \texttt{atom\_inc}, \texttt{atom\_min} a \texttt{atom\_max} jsou pak upravovány jednotlivé čítače v histogramu.

Zpracování souboru o velikosti $2.6$~GiB trvá na mém stroji při použití OpenCL konzistentních $5.5$ sekundy.

\newpage

\section{Uživatelská příručka}

Program \texttt{pprsolver} přijímá tři povinné argumenty v tomto pořadí:

\begin{enumerate}
    \item Cesta ke vstupnímu souboru
    \item Percentil (umožňuje i desetinná čísla)
    \item Způsob zpracování
\end{enumerate}

Jako způsob zpracování lze použít \texttt{single} pro sériové zpracování, \texttt{smp} pro zpracování na symetrickém multiprocesoru nebo název OpenCL zařízení. Rovněž zde může být řetězec \texttt{opencl} pro automatický výběr OpenCL zařízení (prioritně vybírá GPU, ale pokud není k dispozici, hledá libovolný typ zařízení). Třetí argument není citlivý na velikost písmen, tj. \texttt{SINGLE} má stejný význam jako \texttt{single}, \texttt{Single}, či \texttt{SiNgLe} -- to platí i pro název OpenCL zařízení.

Pokud je jako způsob zpracování vybrán SMP, lze programu předat ještě čtvrtý argument, a to počet zpracovacích vláken. Pokud není uveden, je jeho hodnota $4$.


\section{Informace o testování}

Implementovaný program byl testován primárně na stroji s CPU \emph{AMD Ryzen 7 5800X 8-Core} s $32$~GiB RAM a GPU \emph{AMD Radeon R7 260X} s $2$~GiB VRAM. Dále byl program pro kontrolu testován na stroji s CPU \emph{Intel Core i5-6300HQ} s $16$~GiB RAM a GPU \emph{NVidia GTX 960M} se $4$~GiB VRAM.

Program byl na obou strojích testován na operačním systému Manjaro Linux, pro kontrolu pak i na virtuálním stroji s operačním systémem Microsoft Windows.

Soubor o velikosti $2.6$~GiB, který byl použit k testování rychlosti, a je zmiňován v předchozích částech dokumentace, se nacházel na NVMe SSD úložišti a byl jím obraz operačního systému Ubuntu~21.04.

\newpage

\section{Závěr}

Program, který je předmětem této semestrální práce, byl implementován a optimalizován do té míry, že soubor o velikosti jednotek gigabajtů je schopen na moderní pracovní stanici zpracovat v rámci jednotek až nižších desítek sekund. Jeho výsledky byly porovnány s dalšími implementacemi a zdají se být správné.

Program nevykazuje známky problémů se synchronizací, jeho výsledky jsou vždy konzistentní.

Výsledky jsou konzistentní i za použití OpenCL s testovanými grafickými čipy od výrobců AMD a NVidia. Na čipech od výrobce Intel program funguje správně po vypnutí kompilačních optimalizací, které narušují funkci programu, pravděpodobně kvůli použití type punningu.
