#pragma once

#include <string>

namespace kppr::ocl {
    const std::string kernel_src = R"===(

#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable

constant ulong SIGN_MASK = (((ulong) 1) << 63);
constant ulong XSIGN_MASK = ~(((ulong) 1) << 63);


inline long from_sm(const double d)
{
    union { double d; ulong i; long s; } v = { .d = d };
    if (v.s >= 0)
        return v.s;
    else
        return -(v.i & XSIGN_MASK);
}

inline size_t value_to_index(const long nsm, const long bucket_size, const long index_offset)
{
    return (nsm / bucket_size) + index_offset;
}

__kernel void histogram_insert(
        global double* input, ulong input_length, ulong position,
        long range_from, long range_to, long bucket_size, long index_offset,
        global ulong* counts, global ulong* first_positions, global ulong* last_positions,
        global ulong* total_inserted, global ulong* under_range)
{
    // Discard non-normal/non-zero values and make zeroes all the same
    size_t in_index = get_global_id(0);
    double in_d = input[in_index]; // input value
    long nsm = from_sm(in_d);      // input value converted from sign-magnitude to two's complement

    if (!isnormal(in_d) && in_d != 0.0 && in_d != -0.0)
        return;
    if (in_d == -0.0)
        in_d = 0.0;


    atom_inc(total_inserted);

    if (nsm < range_from) {
        atom_inc(under_range);
        return;
    }

    if (nsm > range_to)
        return;

    size_t i = value_to_index(nsm, bucket_size, index_offset);
    atom_inc(&counts[i]);

    ulong inpos = position + in_index * sizeof(double);
    atom_min(&first_positions[i], inpos);
    atom_max(&last_positions[i], inpos);
}

)===";
}
