#pragma once

#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>
#include <mutex>
#include <ostream>
#include <vector>

#include "sync.h"

using std::size_t;
using std::ptrdiff_t;

namespace kppr {

    enum Status : int {
        SUCCESS       = 0x00, ///< All is fine!
        ARGS          = 0x01, ///< Invalid arguments provided
        IO            = 0x02, ///< I/O error (e.g. file not found)
        UNIMPLEMENTED = 0x03, ///< Functionality not implemented
        UNEXPECTED    = 0x04, ///< Unexpected result (a situation that should not happen - a bug)
        NO_DATA       = 0x05, ///< No data in the histogram - invalid input file
        CL            = 0x06  ///< OpenCL error
    };


    /// Bit mask that contains a 1 at the sign bit, the rest is 0
    const uint64_t SIGN_MASK = (((uint64_t) 1) << 63);

    /// Bit mask that contains a 0 at the sign bit, the rest is 1
    const uint64_t XSIGN_MASK = ~SIGN_MASK;


    /// Data value used for type punning of doubles and ints
    struct Value {
        uint64_t content;

        inline Value() {}
        inline Value(double d) { this->set(d); }
        inline Value(int64_t s) { this->set(s); }
        inline Value(uint64_t v) : content(v) {}

        static inline Value min() { return Value(static_cast<uint64_t>(-1)); }
        static inline Value max() { return Value(static_cast<uint64_t>(std::numeric_limits<int64_t>().max())); }

        inline void set(uint64_t v) { content = v; }
        inline void set(int64_t s) { std::memcpy(&content, &s, sizeof(s)); }
        inline void set(double d) { std::memcpy(&content, &d, sizeof(d)); }

        /// Gets the bit content as if they represented a `uint64_t`
        inline uint64_t geti() const { return content; }

        /// Gets the bit content as if they represented an `int64_t`
        inline int64_t gets() const
        {
            int64_t s;
            std::memcpy(&s, &content, sizeof(s));
            return s;
        }

        /// Gets the bit content as if they represented a `double`
        inline double getd() const
        {
            double d;
            std::memcpy(&d, &content, sizeof(d));
            return d;
        }

        /// Converts the bit content to an integer in the two's complement format, as if it originally represented one
        /// in the sign-magnitude format
        inline int64_t from_sm() const
        {
            if (this->gets() >= 0)
                return this->gets();
            else
                return -static_cast<int64_t>(this->geti() & XSIGN_MASK);
        }

        /// Converts `i` (in two's complement format) to the sign-magnitude format, storing it in a new `Value`
        static inline Value to_sm(int64_t i)
        {
            if (i >= 0)
                return { i };
            else {
                assert((i != std::numeric_limits<int64_t>().min())
                        && "The lowest int64_t is not representable in the sign-magnitude format");

                return { static_cast<uint64_t>(-i) | SIGN_MASK };
            }
        }

        inline Value next_bucket_border(uint64_t bucket_size) const
        {
            if (this->geti() >= 0)
                return { this->geti() + bucket_size };
            else {
                uint64_t v = this->geti() & XSIGN_MASK;
                v += bucket_size;
                return { v | SIGN_MASK };
            }
        }
    };

    inline bool operator==(const Value& lhs, const Value& rhs)
    {
        return lhs.geti() == rhs.geti();
    }

    inline bool operator>(const Value& lhs, const Value& rhs)
    {
        return lhs.from_sm() > rhs.from_sm();
    }

    inline bool operator>=(const Value& lhs, const Value& rhs)
    {
        return lhs.from_sm() >= rhs.from_sm();
    }

    inline bool operator<(const Value& lhs, const Value& rhs)
    {
        return lhs.from_sm() < rhs.from_sm();
    }

    inline bool operator<=(const Value& lhs, const Value& rhs)
    {
        return lhs.from_sm() <= rhs.from_sm();
    }


    /// Structure holding result data of the solver.
    struct Solver_Result {
        int status;
        Value value;
        uint64_t count;
        uint64_t first_position;
        uint64_t last_position;
        uint64_t bucket_size;

        static inline Solver_Result err(int status)
        {
            Solver_Result result;
            result.status = status;
            return result;
        }
    };


    /// A single entry of a `Histogram` - only used as a reference for `Histogram` sizing
    struct Entry {
        uint64_t count;
        uint64_t first_position;
        uint64_t last_position;
    };

    /// Maximum memory allowed to be taken by a histogram (in bytes).
    /// Found 2 MiB to be about the optimum for the array histogram.
    const size_t MAX_HISTOGRAM_MEMORY = 2 * 1048576;

    /// Maximum number of entries allowed for a histogram in single processor mode
    const size_t HISTOGRAM_SIZE = MAX_HISTOGRAM_MEMORY / sizeof(Entry);

    /// Size of the buffer into which the input file is read, in bytes.
    const size_t INPUT_BUFFER_SIZE = 1048576;


    /// Histogram of bucketable `Value`s
    template <size_t Size>
    struct Histogram {
        Value range_from;
        Value range_to;
        bool full_range;
        int64_t bucket_size;

        ptrdiff_t index_offset;

        uint64_t under_range;
        uint64_t total_inserted;

        // These should copy Entry, but we are using arrays to make way for auto-vectorization
        std::array<uint64_t, Size> counts;
        std::array<uint64_t, Size> first_positions;
        std::array<uint64_t, Size> last_positions;

        // The histogram needs to be initialized!
        Histogram() = delete;

        /// Initializes the histogram with the specified range. Range borders are swapped if they are the wrong way
        /// around.
        Histogram(Value a_range_from, Value a_range_to)
        {
            reset(a_range_from, a_range_to);
        }


        /// Resets the histogram and reinitializes with the specified range. Range borders are swapped if they are
        /// the wrong way around.
        void reset(Value a_range_from, Value a_range_to)
        {
            if (a_range_from <= a_range_to) {
                this->range_from = a_range_from;
                this->range_to = a_range_to;
            } else {
                this->range_from = a_range_to;
                this->range_to = a_range_from;
            }

            full_range = (this->range_from == Value::min() && this->range_to == Value::max());

            uint64_t range = this->range_to.from_sm() - this->range_from.from_sm();
            this->bucket_size = range / static_cast<int64_t>(Size - 1) + !!(range % static_cast<int64_t>(Size - 1));
            if (this->bucket_size == 0)
                this->bucket_size = 1;
            assert(this->bucket_size > 0);

            this->index_offset = - (this->range_from.from_sm() / this->bucket_size);

            this->under_range = 0;
            this->total_inserted = 0;

            this->counts.fill(0);
        }

        Solver_Result find_percentile(double percentile) {
            assert(percentile >= 0.0 && percentile <= 1.0);

            if (!this->total_inserted)
                return Solver_Result::err(Status::NO_DATA);

            uint64_t target_pos = static_cast<uint64_t>((this->total_inserted - 1) * percentile);
            uint64_t sum = under_range;
            assert((sum <= target_pos) && "Undershot the percentile - this should never happen");
            for (size_t i = 0; i < Size; i++) {
                uint64_t count = this->counts[i];
                sum += count;

                if (sum > target_pos) {
                    Solver_Result result = {};
                    result.value = this->index_to_value(i);
                    result.count = count;
                    result.first_position = this->first_positions[i];
                    result.last_position = this->last_positions[i];
                    result.bucket_size = this->bucket_size;
                    return result;
                }
            }
            assert(false && "Overshot the percentile - this should never happen");
            return Solver_Result::err(Status::UNEXPECTED);
        }

        /// Inserts an entry with the specified input value, automatically bucketing it, if this histogram instance
        /// requires it. The histogram will always be sorted.
        void insert(Value input_value, uint64_t position)
        {
            // Discard non-normal/non-zero values and make zeroes all the same
            auto fpc = std::fpclassify(input_value.getd());
            if (fpc != FP_NORMAL && fpc != FP_ZERO)
                return;
            if (fpc == FP_ZERO)
                input_value.set((double) 0.0);

            this->total_inserted += 1;

            if (!full_range) {
                // Under-range values are discarded but still need to count towards the percentile and the total
                if (input_value < range_from) {
                    this->under_range += 1;
                    return;
                }

                // Over-range values are counted towards the total and discarded, we don't need them at all
                if (input_value > range_to)
                    return;
            }

            // Find the histogram index and increment values
            size_t i = this->value_to_index(input_value);
            assert(i < Size);

            if (this->counts[i] == 0) {
                this->first_positions[i] = position;
                this->last_positions[i] = position;
            } else {
                if (this->first_positions[i] > position)
                    this->first_positions[i] = position;

                if (this->last_positions[i] < position)
                    this->last_positions[i] = position;
            }

            this->counts[i] += 1;
        }

        /// Inserts an entry with the value of the specified double.
        inline void insert(double d, uint64_t position)
        {
            Value v;
            v.set(d);
            insert(v, position);
        }

        /// Inserts an entry with the value of the specified integer.
        inline void insert(uint64_t i, uint64_t position)
        {
            Value v;
            v.set(i);
            insert(v, position);
        }

        /// Converts the specified `Value` to an entry index in this histogram.
        inline size_t value_to_index(Value v)
        {
            int64_t s = v.from_sm();
            int64_t result = (s / this->bucket_size) + this->index_offset;
            assert(result >= 0);
            return result;
        }

        /// Converts the specified index to a `Value` bucketed for this histogram.
        inline Value index_to_value(size_t i)
        {
            int64_t s = (i - this->index_offset) * this->bucket_size;
            return Value::to_sm(s);
        }

        /// Merges the histograms provided by the specified iterators into this one.
        template <typename IteratorType>
        void merge(IteratorType h_start, IteratorType h_end)
        {
            for (IteratorType it = h_start; it != h_end; it++) {
                Histogram<Size>& src = *it;
                assert((this->range_from == src.range_from && this->range_to == src.range_to)
                        && "Histogram ranges must be the same");

                this->total_inserted += src.total_inserted;
                this->under_range += src.under_range;

                for (size_t i = 0; i < Size; i++) {
                    if (!src.counts[i])
                        continue;

                    if (this->counts[i]) {
                        if (src.first_positions[i] < this->first_positions[i])
                            this->first_positions[i] = src.first_positions[i];
                        if (src.last_positions[i] > this->last_positions[i])
                            this->last_positions[i] = src.last_positions[i];
                    } else {
                        this->first_positions[i] = src.first_positions[i];
                        this->last_positions[i] = src.last_positions[i];
                    }

                    this->counts[i] += src.counts[i];
                }
            }
        }
    };

    /// Buffer of read doubles. `Buffer_Size` is the size of the buffer in doubles.
    template <size_t Buffer_Size>
    struct Data_Buf {
        double buffer[Buffer_Size];  ///< The data buffer for reading
        size_t filled;               ///< Number of filled doubles in the buffer
        uint64_t position;           ///< Position of the file (in bytes) on which this buffer starts
        bool eof;                    ///< Whether reading into this buffer has reached EOF
    };


    /// Circular data provider. Implements the producer-consumer mechanics.
    template <size_t Data_Size, size_t C_Size>
    class Circular {
        sync::Semaphore<C_Size> filled_sem;
        sync::Semaphore<C_Size> empty_sem;

        std::mutex dm;
        std::mutex cm;
        size_t offset = 0;
        size_t filled = 0;

        Data_Buf<Data_Size> tbuffer[C_Size];

        public:

        // Yea, because we deleted the copy constructor, we now have to explicitly define the default constructor,
        // makes sense, no? C++ is dumb.
        Circular() : filled_sem(0), empty_sem(C_Size) {}

        // no copying!
        Circular(Circular&) = delete;
        Circular& operator=(Circular&) = delete;

        /// Produces a new entry in the circular buffer, using the specified `producer` function to initialize it.
        /// If the circular buffer is full, it blocks until space is freed up by consumers.
        void produce(std::function<void (Data_Buf<Data_Size>& t)> producer)
        {
            auto t = std::make_unique<Data_Buf<Data_Size>>();
            producer(*t);

            empty_sem.acquire();
            {
                std::lock_guard l(dm);
                tbuffer[(offset + filled) % C_Size] = *t;
                filled++;
            }
            filled_sem.release();
        }

        /// Takes an entry from the circular buffer and passes it to the `consumer` function. If the circular buffer is
        /// empty, it blocks until the producer adds something to it.
        void consume(std::function<void (const Data_Buf<Data_Size>& t)> consumer)
        {
            auto t = std::make_unique<Data_Buf<Data_Size>>();

            filled_sem.acquire();
            {
                std::lock_guard l(dm);
                *t = tbuffer[offset];
                filled--;
                offset = (offset + 1) % C_Size;
            }
            empty_sem.release();

            consumer(*t);
        }

        /// Clears out the circular buffer so that it is ready for a new phase. This operation is not thread-safe,
        /// it may only be called when at most one thread is operating on this circular buffer!
        void clear()
        {
            std::lock_guard l(dm);
            while (filled) {
                while (filled_sem.try_acquire()) {
                    empty_sem.release();
                    filled--;
                }
            }
            assert(filled == 0);
        }
    };

}

