#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

#include "solver_single.h"
#include "solver_smp.h"
#include "solver_opencl.h"

#include "data.h"


namespace kppr {

    /// The type of processor to use for the calculation
    enum class Proc_Type {
        UNKNOWN = 0,
        SINGLE  = 1,
        SMP     = 2,
        OPENCL  = 3
    };

    /// A map, mapping string representations to the actual values of `Proc_Type`
    #define X(val) { #val, Proc_Type::val }
    static const std::map<std::string, Proc_Type> string_to_proc_type_map = {
        X(SINGLE),
        X(SMP),
        X(OPENCL)
    };
    #undef X

    /// Converts the specified string to a `Proc_Type` value. If the string does not match a `Proc_Type` value,
    /// `Proc_Type::UNKNOWN` is returned.
    static Proc_Type string_to_proc_type(const std::string& str)
    {
        const auto it = string_to_proc_type_map.find(str);
        return (it == string_to_proc_type_map.cend())
            ? Proc_Type::UNKNOWN
            : it->second;
    }

    /// Program configuration
    struct Config {
        std::string infile;
        double percentile;
        Proc_Type proc_type;
        size_t worker_count;
        std::string cl_device_name;

        void parse_args(int argc, char** argv)
        {
            if (argc < 4)
                throw std::invalid_argument("Expected at least 3 arguments");

            if (argc > 5)
                throw std::invalid_argument("Expected at most 4 arguments");

            infile = std::string(argv[1]);

            try {
                // percentile is divided by 100 because that's how maths is gonna be done anyway
                percentile = std::stod(std::string(argv[2])) / 100.0;
            } catch (...) {
                throw std::invalid_argument("Percentile must be a real number");
            }

            if (percentile < 0.0 || percentile > 1.0) {
                throw std::invalid_argument("Percentile must be between 0 and 100");
            }

            std::string proc_type_str(argv[3]);
            std::transform(proc_type_str.begin(), proc_type_str.end(), proc_type_str.begin(), toupper);
            proc_type = string_to_proc_type(proc_type_str);
            if (proc_type == Proc_Type::UNKNOWN) {
                proc_type = Proc_Type::OPENCL;
                cl_device_name = argv[3];
            }

            if (argc < 5)
                worker_count = 4;
            else {
                if (proc_type != Proc_Type::SMP)
                    throw std::invalid_argument("Worker count is only applicable to SMP mode");

                try {
                    worker_count = std::stoull(argv[4]);
                } catch (...) {
                    throw std::invalid_argument("Worker count must be an integer");
                }

                if (worker_count == 0)
                    throw std::invalid_argument("There must be at least one worker");
            }
        }
    };


    static inline Solver_Result solve(Proc_Type proc_type,
                                      std::istream& in,
                                      double percentile,
                                      size_t worker_count,
                                      std::string& cl_device_name)
    {
        switch (proc_type) {
            case Proc_Type::SINGLE:  return single::solve(in, percentile);
            case Proc_Type::SMP:     return smp::solve(in, percentile, worker_count);
            case Proc_Type::OPENCL:  return ocl::solve(in, percentile, cl_device_name);

            default: return Solver_Result::err(Status::UNIMPLEMENTED);
        }
    }

}


int main(int argc, char** argv) {
    kppr::Config c;

    try {
        c.parse_args(argc, argv);
    } catch (const std::invalid_argument& err) {
        std::cerr
            << "USAGE: " << argv[0] << " <file> <percentile> <processor/cl_device> [smp_workers]" << std::endl
            << err.what() << std::endl;
        return kppr::Status::ARGS;
    }

    std::ifstream in(c.infile, std::ios::binary);
    if (!in.is_open()) {
        std::cerr << "Could not open file '" << c.infile << "'" << std::endl;
        return kppr::Status::IO;
    }

    kppr::Solver_Result result = kppr::solve(c.proc_type, in, c.percentile, c.worker_count, c.cl_device_name);

    if (result.status) {
        if (result.status == kppr::Status::UNIMPLEMENTED)
            std::cerr << "Solver error - Unimplemented" << std::endl;
        if (result.status == kppr::Status::NO_DATA)
            std::cerr << "Solver error - The input file contained no suitable data" << std::endl;
        else
            std::cerr << "Solver error " << std::hex << result.status << std::endl;

        return result.status;
    }

    std::cout << std::hexfloat << result.value.getd() << " "
        << result.first_position << " "
        << result.last_position << std::endl;

    return kppr::Status::SUCCESS;
}


