#include <limits>
#include <memory>

#include "data.h"
#include "watchdog.h"

#include "solver_single.h"


namespace kppr::single {

    struct Data_Holder {
        Histogram<HISTOGRAM_SIZE> histogram;
        double buffer[INPUT_BUFFER_SIZE / sizeof(double)];

        Data_Holder() : histogram(Value::min(), Value::max()) {}
    };

    Solver_Result solve(std::istream& in, double percentile)
    {
        Watchdog watchdog;

        // It actually is VERY important to have the histogram and the buffer together in a struct like this. If we just
        // use two unique_ptrs, we get a performance penalty due to page faults!
        auto data = std::make_unique<Data_Holder>();
        Solver_Result result;

        // Histogram building loop
        do {
            in.clear();
            in.seekg(0);

            size_t b_read;
            uint64_t pos = 0;

            // Reading loop
            while (!in.eof()) {
                in.read(reinterpret_cast<char*>(data->buffer), sizeof(data->buffer));
                b_read = in.gcount();
                size_t d_read = b_read / sizeof(double);

                for (size_t i = 0; i < d_read; i++) {
                    data->histogram.insert(data->buffer[i], pos + i * sizeof(double));
                }

                pos += b_read;
                watchdog.report();
            }

            result = data->histogram.find_percentile(percentile);
            watchdog.report();
            if (result.status) // on error
                return result;

            // We are still bucketed, let's repeat on a smaller range
            if (result.bucket_size > 1) {
                Value from = result.value;
                Value to = from.next_bucket_border(result.bucket_size);
                data->histogram.reset(from, to);
                watchdog.report();
            }
        } while (result.bucket_size > 1);

        return result;
    }

}

