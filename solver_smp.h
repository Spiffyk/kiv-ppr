#pragma once

#include <istream>

#include "data.h"


namespace kppr::smp {

    /// Reads the stream and finds the specified percentile in it. It uses the specified number of worker threads
    /// to do so.
    Solver_Result solve(std::istream& in, double percentile, size_t num_threads = 4);

}
