#include <memory>
#include <thread>

#include "data.h"
#include "sync.h"
#include "watchdog.h"

#include "solver_smp.h"


namespace kppr::smp {

    using Circ_Buf = Circular<INPUT_BUFFER_SIZE / sizeof(double), 32>;

    struct Data_Holder {
        Histogram<HISTOGRAM_SIZE> main_histogram;
        std::vector<Histogram<HISTOGRAM_SIZE>> thread_histograms;
        std::vector<Circ_Buf> circulars;

        Data_Holder(size_t num_threads) :
            main_histogram(Value::min(), Value::max()),
            circulars(num_threads)
        {
            for (size_t i = 0; i < num_threads; i++) {
                thread_histograms.emplace_back(Value::min(), Value::max());
            }
        }
    };


    Solver_Result solve(std::istream& in, double percentile, size_t num_threads)
    {
        Watchdog watchdog;

        // Physical position of data in memory. Important. (See `solver_single.cpp`)
        auto data = std::make_unique<Data_Holder>(num_threads);

        Solver_Result result;
        std::atomic<bool> cont(true);
        sync::Barrier barrier(num_threads + 1, [&]() noexcept {
            data->main_histogram.merge(data->thread_histograms.begin(), data->thread_histograms.end());
            result = data->main_histogram.find_percentile(percentile);
            watchdog.report();

            // Clear the circular buffers of any excess EOFs
            for (auto& c : data->circulars)
                c.clear();

            if (result.status) { // on error
                cont = false;
                return;
            }

            // We are still bucketed, let's repeat on a smaller range
            if (result.bucket_size > 1) {
                Value from = result.value;
                Value to = from.next_bucket_border(result.bucket_size);
                data->main_histogram.reset(from, to);
                for (auto& h : data->thread_histograms)
                    h.reset(from, to);
                watchdog.report();
            }
        });

        // Worker thread procedure
        auto thread_solve = [&watchdog, &cont, &barrier, &result](Histogram<HISTOGRAM_SIZE>& histogram,
                                                                  Circ_Buf& circular)
        {
            do {
                bool eof = false;
                while (!eof) {
                    circular.consume([&](auto& buf) {
                        for (size_t i = 0; i < buf.filled; i++) {
                            histogram.insert(buf.buffer[i], buf.position + (i * sizeof(double)));
                        }
                        eof = buf.eof;
                    });
                    watchdog.report();
                }

                barrier.arrive_and_wait();
            } while (cont && result.bucket_size > 1);
        };

        // Create worker threads
        std::vector<std::thread> workers;
        for (size_t i = 0; i < num_threads; i++) {
            workers.emplace_back(thread_solve, std::ref(data->thread_histograms[i]), std::ref(data->circulars[i]));
        }

        // Data reading loop
        do {
            in.clear();
            in.seekg(0);

            size_t buf_i = 0;
            size_t b_read;
            uint64_t pos = 0;

            // Put data round-robin into the threads' circular buffers until the end of the file
            while (!in.eof()) {
                data->circulars[buf_i].produce([&](auto& buf) {
                    in.read(reinterpret_cast<char*>(buf.buffer), sizeof(buf.buffer));
                    b_read = in.gcount();
                    buf.filled = b_read / sizeof(double);
                    buf.position = pos;
                    buf.eof = in.eof();
                });

                pos += b_read;
                buf_i = (buf_i + 1) % num_threads;
                watchdog.report();
            }

            // Add EOFs into the circular buffers for all threads, so that they can gracefully end the current phase
            for (auto& c : data->circulars) {
                c.produce([&](auto& buf) {
                    buf.filled = 0;
                    buf.position = pos;
                    buf.eof = true;
                });
                watchdog.report();
            }

            barrier.arrive_and_wait();
        } while (cont && result.bucket_size > 1);

        // Cleanup worker threads
        for (auto& worker : workers) {
            worker.join();
        }

        return result;
    }

}

