#include <chrono>
#include <cstdlib>
#include <iostream>

#include "watchdog.h"

/// Time of inactivity after which the watchdog will start printing warnings
static const std::chrono::milliseconds WARN_TIMEOUT = std::chrono::milliseconds(500);

/// Time of inactivity after which the watchdog will call `std::abort()`
static const std::chrono::milliseconds ABORT_TIMEOUT = std::chrono::milliseconds(15000);


kppr::Watchdog::Watchdog() : thread([this] { this->thrproc(); }),
                             running(true),
                             counter(0)
{}

kppr::Watchdog::~Watchdog()
{
    this->report();
    this->running = false;
    this->cond.notify_all();
    this->thread.join();
}

void kppr::Watchdog::report()
{
    this->counter++;
}

void kppr::Watchdog::thrproc()
{
    std::unique_lock lock(this->mutex);
    auto last_activity = std::chrono::high_resolution_clock().now();
    uint64_t last_counter = this->counter;

    while (running) {
        auto now = std::chrono::high_resolution_clock().now();
        if (this->counter == last_counter) {
            // Counter has not changed - check the time
            auto diff = now - last_activity;
            if (diff > WARN_TIMEOUT) {
                if (diff > ABORT_TIMEOUT) {
                    std::cerr << "WATCHDOG: No activity registered in a long time. Aborting." << std::endl;
                    std::abort();
                } else {
                    std::cerr << "WATCHDOG: Warning: No activity is being reported." << std::endl;
                }
            }
        } else {
            // Counter has changed - activity is being recorded
            last_activity = now;
            last_counter = this->counter;
        }

        this->cond.wait_for(lock, std::chrono::milliseconds(100));
    }
}

