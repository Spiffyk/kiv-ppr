#pragma once

#include <istream>

#include "data.h"


namespace kppr::single {

    /// Reads the stream and finds the specified percentile in it.
    Solver_Result solve(std::istream& in, double percentile);

}

