#pragma once

#include <atomic>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace kppr {

    class Watchdog {
        std::thread thread;
        std::mutex mutex;
        std::condition_variable cond;

        std::atomic<bool> running;
        std::atomic<uint64_t> counter;

    public:

        // Starts a new watchdog
        Watchdog();

        // Ends the watchdog
        ~Watchdog();

        // Reports activity to the watchdog - a.k.a. chill out, things are moving
        void report();


    private:

        // The procedure running in the watchdog thread
        void thrproc();

    };

}

