#pragma once

#include <istream>

#include "data.h"


namespace kppr::ocl {

    /// Reads the stream and finds the specified percentile in it. It uses the specified OpenCL platform and device
    /// to do so. Only GPU devices are supported.
    Solver_Result solve(std::istream& in, double percentile, std::string& cl_device_name);

}

