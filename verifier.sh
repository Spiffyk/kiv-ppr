#!/bin/bash

# Test file: https://releases.ubuntu.com/21.04/ubuntu-21.04-desktop-amd64.iso

set -e

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

cmake --build .

for i in {1..99}
do
  expected=$(sed -n ${i}'p' "$SCRIPT_DIR/verifier_1_99.txt")
  echo "PERCENTILE: ${i}:"

  if diff <(echo $expected) <(./pprsolver $1 $i $2 $3) ; then
    echo "ok"
  else
    echo "nok"
  fi
done

