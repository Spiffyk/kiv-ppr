#define CL_HPP_TARGET_OPENCL_VERSION 200

#if __has_include(<CL/opencl.hpp>)
    #include <CL/opencl.hpp>
#else
    #include "opencl/vendor/opencl.hpp"
#endif

#include <algorithm>
#include <iostream>
#include <vector>

#include "data.h"
#include "opencl/cl_kernel_src.h"
#include "watchdog.h"

#include "solver_opencl.h"

namespace kppr::ocl {

    struct Data_Holder {
        cl_double buffer[INPUT_BUFFER_SIZE / sizeof(double)];
        cl_ulong counts[HISTOGRAM_SIZE];
        cl_ulong first_positions[HISTOGRAM_SIZE];
        cl_ulong last_positions[HISTOGRAM_SIZE];
    };

    /// Gets an OpenCL GPU device. If a device is found, it is assigned to `out_device` and the function
    /// returns `Status::SUCCESS`. If an error occurs, it is printed out and an appropriate non-zero `Status` value
    /// is returned.
    static Status get_default_device(cl::Device& out_device)
    {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if (platforms.empty()) {
            std::cerr << "No suitable OpenCL platforms found" << std::endl;
            return Status::CL;
        }
        cl::Platform platform = platforms[0];

        std::vector<cl::Device> devices;
        platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
        if (devices.empty()) {
            std::cerr << "No suitable OpenCL devices found" << std::endl;
            return Status::CL;
        }

        // Prioritize GPUs
        for (auto& dev : devices) {
            if (dev.getInfo<CL_DEVICE_TYPE>() == CL_DEVICE_TYPE_GPU) {
                out_device = dev;
                return Status::SUCCESS;
            }
        }

        // If no GPU is found, just use whatever is available
        out_device = devices[0];
        return Status::SUCCESS;
    }

    static Status get_device(cl::Device& out_device, std::string& cl_device_name)
    {
        if (cl_device_name.empty())
            return get_default_device(out_device);

        std::string search_name = cl_device_name;
        std::transform(search_name.begin(), search_name.end(), search_name.begin(), ::tolower);

        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if (platforms.empty()) {
            std::cerr << "No suitable OpenCL platforms found" << std::endl;
            return Status::CL;
        }

        for (auto& platform : platforms) {
            std::vector<cl::Device> devices;
            platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

            for (auto& device : devices) {
                std::string dev_name = device.getInfo<CL_DEVICE_NAME>();
                std::transform(dev_name.begin(), dev_name.end(), dev_name.begin(), ::tolower);
                if (search_name == dev_name) {
                    out_device = device;
                    return Status::SUCCESS;
                }
            }
        }

        std::cerr << "No OpenCL device named '" << cl_device_name << "' found." << std::endl;
        return Status::ARGS;
    }

    /// Gets bucket size from range. `range_from` and `range_to` are two's complement signed integers.
    static inline cl_ulong get_bucket_size(cl_long range_from, cl_long range_to)
    {
        uint64_t range = range_to - range_from;
        return range / static_cast<int64_t>(HISTOGRAM_SIZE - 1) + !!(range % static_cast<int64_t>(HISTOGRAM_SIZE - 1));
    }

    /// Gets index offset from range and bucket size. `range_from` is a two's complement signed integer.
    static inline cl_long get_index_offset(cl_long range_from, cl_long bucket_size)
    {
        return - (range_from / bucket_size);
    }

    static inline Value index_to_value(size_t i, cl_long index_offset, cl_long bucket_size)
    {
        int64_t s = (i - index_offset) * bucket_size;
        return Value::to_sm(s);
    }

    static bool is_intel(cl::Device dev)
    {
        std::string vendor(dev.getInfo<CL_DEVICE_VENDOR>());
        std::transform(vendor.begin(), vendor.end(), vendor.begin(), ::tolower);
        return vendor.find("intel") != std::string::npos;
    }

    static Solver_Result find_percentile(const Data_Holder& data, cl_ulong total_inserted, cl_ulong under_range,
                                         cl_long index_offset, cl_long bucket_size, double percentile)
    {
        assert(percentile >= 0.0 && percentile <= 1.0);

        if (!total_inserted)
            return Solver_Result::err(Status::NO_DATA);

        cl_ulong target_pos = static_cast<cl_ulong>((total_inserted - 1) * percentile);
        cl_ulong sum = under_range;
        assert((sum <= target_pos) && "Undershot the percentile - this should never happen");
        for (size_t i = 0; i < HISTOGRAM_SIZE; i++) {
            uint64_t count = data.counts[i];
            sum += count;

            if (sum > target_pos) {
                Solver_Result result = {};
                result.value = index_to_value(i, index_offset, bucket_size);
                result.count = count;
                result.first_position = data.first_positions[i];
                result.last_position = data.last_positions[i];
                result.bucket_size = bucket_size;
                return result;
            }
        }
        assert(false && "Overshot the percentile - this should never happen");
        return Solver_Result::err(Status::UNEXPECTED);
    }


    Solver_Result solve(std::istream& in, double percentile, std::string& cl_device_name)
    {
        Watchdog watchdog;

        cl::Device dev;
        Status dev_status = get_device(dev, cl_device_name);
        if (dev_status)
            return Solver_Result::err(dev_status);

        cl::Context ctx = cl::Context(dev);

        // Prepare the GPU program and command queue
        cl::Program program(ctx, kernel_src);

        std::string build_flags = "";
        // Optimizations are disabled for Intel, because their chips yield incorrect results otherwise
        if (is_intel(dev))
            build_flags += "-cl-opt-disable";

        cl_int build_status = program.build(dev, build_flags.c_str());
        std::string build_log = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);


        if (build_status != CL_SUCCESS) {
            std::cerr << "Could not build OpenCL kernel:\n" << build_log  << std::endl;
            return Solver_Result::err(Status::CL);
        } else if (!build_log.empty() && !std::all_of(build_log.begin(), build_log.end(), ::isspace)) {
            std::cerr << "OpenCL kernel build output:\n" << build_log << std::endl;
        }
        watchdog.report();

        cl::CommandQueue queue = cl::CommandQueue(ctx);
        cl::KernelFunctor<
                cl::Buffer /*input*/, cl_ulong /*input_length*/, cl_ulong /*position*/,
                cl_long /*range_from*/, cl_long /*range_to*/, cl_long /*bucket_size*/, cl_long /*index_offset*/,
                cl::Buffer /*counts*/, cl::Buffer /*first_positions*/, cl::Buffer /*last_positions*/,
                cl::Buffer /*total_inserted*/, cl::Buffer /*under_range*/
            > histogram_insert(program, "histogram_insert");

        // Prepare memory buffers for use by the GPU
        // - Input file data
        cl::Buffer input_mem(ctx, CL_MEM_READ_WRITE,  INPUT_BUFFER_SIZE);

        // - Histogram params
        cl_long range_from = Value::min().from_sm();
        cl_long range_to   = Value::max().from_sm();
        cl_long bucket_size;
        cl_long index_offset;

        // - Histogram output data
        cl::Buffer counts_mem         (ctx, CL_MEM_READ_WRITE, sizeof(Data_Holder::counts));
        cl::Buffer first_pos_mem      (ctx, CL_MEM_READ_WRITE, sizeof(Data_Holder::first_positions));
        cl::Buffer last_pos_mem       (ctx, CL_MEM_READ_WRITE, sizeof(Data_Holder::last_positions));
        cl::Buffer total_inserted_mem (ctx, CL_MEM_READ_WRITE, sizeof(cl_ulong));
        cl::Buffer under_range_mem    (ctx, CL_MEM_READ_WRITE, sizeof(cl_ulong));
        watchdog.report();

        // Data 'togetherness' is probably not that important in the OpenCL part, as is gets copied over to the GPU
        // anyway, but it's still too big for the stack, so why not put it together on the heap and spare ourselves
        // multiple allocations. Plus it still might give us some marginal performance gains.
        auto data = std::make_unique<Data_Holder>();
        Solver_Result result;

        do {
            // Initialize histogram
            if (range_to < range_from)
                std::swap(range_to, range_from);
            bucket_size = get_bucket_size(range_from, range_to);
            index_offset = get_index_offset(range_from, bucket_size);
            const cl_ulong zero = 0;
            std::memset(data->counts, 0, sizeof(data->counts));
            std::memset(data->first_positions, 255, sizeof(data->first_positions));
            std::memset(data->last_positions, 0, sizeof(data->last_positions));

            // Write initialized histogram to GPU
            queue.enqueueWriteBuffer(counts_mem, CL_FALSE, 0, sizeof(Data_Holder::counts), data->counts);
            queue.enqueueWriteBuffer(first_pos_mem, CL_FALSE, 0, sizeof(Data_Holder::first_positions), data->first_positions);
            queue.enqueueWriteBuffer(last_pos_mem, CL_FALSE, 0, sizeof(Data_Holder::last_positions), data->last_positions);
            queue.enqueueWriteBuffer(total_inserted_mem, CL_FALSE, 0, sizeof(cl_ulong), &zero);
            queue.enqueueWriteBuffer(under_range_mem, CL_FALSE, 0, sizeof(cl_ulong), &zero);
            watchdog.report();

            in.clear();
            in.seekg(0);

            cl_ulong pos = 0;

            while (!in.eof()) {
                in.read(reinterpret_cast<char*>(data->buffer), sizeof(data->buffer));
                size_t b_read = in.gcount();
                cl_ulong d_read = b_read / sizeof(cl_double);

                queue.enqueueWriteBuffer(input_mem, CL_TRUE, 0, sizeof(Data_Holder::buffer), data->buffer);

                cl::EnqueueArgs eargs(queue, cl::NDRange(d_read));
                histogram_insert(eargs,
                        input_mem, d_read, pos,
                        range_from, range_to, bucket_size, index_offset,
                        counts_mem, first_pos_mem, last_pos_mem,
                        total_inserted_mem, under_range_mem);

                pos += b_read;
                watchdog.report();
            }

            // Collect built histogram from GPU
            cl_ulong total_inserted;
            cl_ulong under_range;
            queue.enqueueReadBuffer(counts_mem, CL_FALSE, 0, sizeof(Data_Holder::counts), data->counts);
            queue.enqueueReadBuffer(first_pos_mem, CL_FALSE, 0, sizeof(Data_Holder::first_positions), data->first_positions);
            queue.enqueueReadBuffer(last_pos_mem, CL_FALSE, 0, sizeof(Data_Holder::last_positions), data->last_positions);
            queue.enqueueReadBuffer(total_inserted_mem, CL_FALSE, 0, sizeof(cl_ulong), &total_inserted);
            queue.enqueueReadBuffer(under_range_mem, CL_FALSE, 0, sizeof(cl_ulong), &under_range);
            watchdog.report();

            queue.finish();
            watchdog.report();

            result = find_percentile(*data, total_inserted, under_range, index_offset, bucket_size, percentile);
            if (result.status) // on error
                return result;

            if (result.bucket_size > 1) {
                Value from = result.value;
                Value to = from.next_bucket_border(bucket_size);
                range_from = from.from_sm();
                range_to = to.from_sm();
                watchdog.report();
            }
        } while (result.bucket_size > 1);

        return result;
    }

}

